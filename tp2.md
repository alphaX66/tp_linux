1. Install MariaDB
🌞 Installer MariaDB sur la machine db.tp2.cesi

```bash
[martint@db ~]$ sudo dnf install mariadb-server
[sudo] password for martint:
Last metadata expiration check: 3:19:50 ago on Tue 14 Dec 2021 10:34:36 AM CET.
Dependencies resolved.
======================================================================================================================== Package                           Architecture  Version                                         Repository        Size
========================================================================================================================Installing:
 mariadb-server                    x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream         16 M
Installing dependencies:
 mariadb                           x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream        6.0 M
 mariadb-common                    x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream         62 k
 mariadb-connector-c               x86_64        3.1.11-2.el8_3                                  appstream        199 k
 mariadb-connector-c-config        noarch        3.1.11-2.el8_3                                  appstream         14 k
 mariadb-errmsg                    x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream        233 k
 perl-Carp                         noarch        1.42-396.el8                                    baseos            29 k
 perl-DBD-MySQL                    x86_64        4.046-3.module+el8.4.0+577+b8fe2d92             appstream        155 k
 perl-DBI                          x86_64        1.641-3.module+el8.4.0+509+59a8d9b3             appstream        739 k
[...]
  perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64
  psmisc-23.1-5.el8.x86_64

Complete!
```


🌞 Le service MariaDB

```
[martint@db ~]$ sudo systemctl start mariadb
[sudo] password for martint:
[martint@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 14:00:55 CET; 21s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 5076 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 4941 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, statu>
  Process: 4916 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 5044 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 17672)
   Memory: 84.6M
   CGroup: /system.slice/mariadb.service
           └─5044 /usr/libexec/mysqld --basedir=/usr

Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: See the MariaDB Knowledgebase at http://ma>
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: MySQL manual for more instructions.
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: Please report any problems at http://maria>
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: The latest information about MariaDB is av>
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: You can find additional information about >
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: http://dev.mysql.com
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: Consider joining MariaDB's strong and vibr>
Dec 14 14:00:45 db.tp2.cesi mysql-prepare-db-dir[4941]: https://mariadb.org/get-involved/
Dec 14 14:00:45 db.tp2.cesi mysqld[5044]: 2021-12-14 14:00:45 0 [Note] /usr/libexec/mysqld (mysqld>
Dec 14 14:00:55 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.
```

```
[martint@db ~]$ sudo ss -ntlup
Netid   State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port   Process
tcp     LISTEN   0        128              0.0.0.0:22            0.0.0.0:*       users:(("sshd",pid=929,fd=5))
tcp     LISTEN   0        80                     *:3306                *:*       users:(("mysqld",pid=5044,fd=21))
tcp     LISTEN   0        128                 [::]:22               [::]:*       users:(("sshd",pid=929,fd=7))
```

```
[martint@db ~]$ ps aux --forest
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           2  0.0  0.0      0     0 ?        S    13:38   0:00 [kthreadd]
[...]
martint     1515  0.0  0.1 162192  5024 ?        S    13:39   0:00  \_ (sd-pam)
mysql       5044  0.0  3.2 1758480 91916 ?       Ssl  14:00   0:00 /usr/libexec/mysqld --basedir=/usr
root        5118  0.0  0.0 232556  2004 ?        Ss   14:01   0:00 /usr/sbin/anacron -s
```



🌞 Firewall


```
[martint@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[martint@db ~]$ sudo
sudo        sudoedit    sudoreplay
[martint@db ~]$ sudo firewall-cmd --reload
success
[martint@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[martint@db ~]$
```


2. Conf MariaDB
Première étape : le mysql_secure_installation. C'est un binaire (= une commande, une application, un programme, ces mots désignent la même chose) qui sert à effectuer des configurations très récurrentes, on fait ça sur toutes les bases de données à l'install.
C'est une question de sécu.
🌞 Configuration élémentaire de la base


```
[martint@db ~]$ sudo mysql_secure_installation
[sudo] password for martint:

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```



🌞 Préparation de la base en vue de l'utilisation par NextCloud

pour ça, il faut vous connecter à la base
il existe un utilisateur root dans la base de données, qui a tous les droits sur la base

ce n'est pas le même root que sur l'OS, c'est celui qui existe dans la base de données


si vous avez correctement répondu aux questions de mysql_secure_installation, vous ne pouvez utiliser le user root de la base de données qu'en vous connectant localement à la base

c'est à dire, pas à distance, pas depuis le réseau


donc, sur la VM db.tp2.cesi toujours :


# Connexion à la base de données
# L'option -p indique que vous allez saisir un mot de passe
# Vous l'avez défini dans le mysql_secure_installation
$ sudo mysql -u root -p

```
[martint@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 18
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'192.168.171.2' IDENTIFIED BY 'dbc'
```


```
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+---------------+
| User      | Host          |
+-----------+---------------+
| root      | 127.0.0.1     |
| nextcloud | 192.168.171.2 |
| root      | ::1           |
| root      | localhost     |
+-----------+---------------+
4 rows in set (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'192.168.171.2';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```



3. Test



🌞 Installez sur la machine web.tp2.cesi la commande mysql



```

[martint@web ~]$ dnf provides mysql
Last metadata expiration check: 0:06:56 ago on Tue 14 Dec 2021 02:47:13 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : @System
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```



🌞 Tester la connexion


```
[martint@web ~]$ mysql -h 192.168.171.3 -u nextcloud -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 20
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
Empty set (0.00 sec)
```



II. Setup Apache

La section II et III sont clairement inspirés de la doc officielle de Rocky pour installer NextCloud.

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP5. Histoire de varier les plaisirs è_é


1. Install Apache

A. Apache
🌞 Installer Apache sur la machine web.tp2.cesi


```
[martint@web ~]$ sudo dnf install httpd
[sudo] password for martint:
Sorry, try again.
[sudo] password for martint:
Last metadata expiration check: 0:49:03 ago on Tue 14 Dec 2021 02:07:14 PM CET.
Dependencies resolved.
========================================================================================================================================
 Package                         Architecture         Version                                             Repository               Size
========================================================================================================================================
Installing:
[...]
  apr-1.6.3-12.el8.x86_64                                          apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64                                apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64               httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64         mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```




🌞 Analyse du service Apache


```
[martint@web ~]$ sudo systemctl start httpd
[martint@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```


```
[martint@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 14:59:49 CET; 4min 49s ago
     Docs: man:httpd.service(8)
 Main PID: 2621 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 17672)
   Memory: 31.8M
   CGroup: /system.slice/httpd.service
           ├─2621 /usr/sbin/httpd -DFOREGROUND
           ├─2622 /usr/sbin/httpd -DFOREGROUND
           ├─2623 /usr/sbin/httpd -DFOREGROUND
           ├─2624 /usr/sbin/httpd -DFOREGROUND
           └─2625 /usr/sbin/httpd -DFOREGROUND

Dec 14 14:59:39 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 14 14:59:49 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 14 14:59:55 web.tp2.cesi httpd[2621]: Server configured, listening on: port 80
```


```
[martint@web ~]$ ps -aux --forest
[...]
root        2621  0.0  0.4 282936 11668 ?        Ss   14:59   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2622  0.0  0.3 296820  8640 ?        S    14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
apache      2623  0.0  0.4 1944496 14296 ?       Sl   14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
apache      2624  0.0  0.6 1813356 18368 ?       Sl   14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
apache      2625  0.0  0.4 1813356 12248 ?       Sl   14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
```

```
[martint@web ~]$ sudo ss -tunlp
[sudo] password for martint:
Netid          State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port          Process
tcp            LISTEN          0               128                            0.0.0.0:22                          0.0.0.0:*              users:(("sshd",pid=928,fd=5))
tcp            LISTEN          0               128                                  *:80                                *:*              users:(("httpd",pid=2625,fd=4),("httpd",pid=2624,fd=4),("httpd",pid=2623,fd=4),("httpd",pid=2621,fd=4))
tcp            LISTEN          0               128                               [::]:22                             [::]:*              users:(("sshd",pid=928,fd=7))
```


```
[martint@web ~]$ ps -aux --forest
apache      2622  0.0  0.3 296820  8640 ?        S    14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
apache      2623  0.0  0.4 1944496 14296 ?       Sl   14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
apache      2624  0.0  0.6 1813356 18368 ?       Sl   14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
apache      2625  0.0  0.4 1813356 12248 ?       Sl   14:59   0:00  \_ /usr/sbin/httpd -DFOREGROUND
```


🌞 Un premier test


```
[martint@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[martint@web ~]$ firewall-cmd --add-port=80/tcp --permanent
Authorization failed.
    Make sure polkit agent is running or run the application as superuser.
[martint@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[martint@web ~]$ sudo firewall-cmd --reload
success
[martint@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
```

```
[martint@web ~]$ curl 192.168.171.2
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
[...]

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```



B. PHP
NextCloud a besoin d'une version bien spécifique de PHP.
Suivez scrupuleusement les instructions qui suivent pour l'installer.
🌞 Installer PHP


```
[martint@web ~]$ sudo dnf install epel-release
[sudo] password for martint:
Sorry, try again.
[sudo] password for martint:
Last metadata expiration check: 1:16:24 ago on Tue 14 Dec 2021 02:07:14 PM CET.
Dependencies resolved.
========================================================================================================================================
 Package                             Architecture                  Version                          Repository                     Size
========================================================================================================================================
Installing:
 epel-release                        noarch                        8-13.el8                         extras                         23 k

Transaction Summary
========================================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                                        203 kB/s |  23 kB     00:00
----------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                    69 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                1/1
  Installing       : epel-release-8-13.el8.noarch                                                                                   1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                                   1/1
/sbin/ldconfig: File /lib64/libwbclient.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libwbclient.so.0.15 is empty, not checked.
/sbin/ldconfig: File /lib64/libdcerpc-binding.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libdcerpc-binding.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libdcerpc-server-core.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libdcerpc-server-core.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libdcerpc.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libdcerpc.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr-krb5pac.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr-krb5pac.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr-nbt.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr-nbt.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr-standard.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr-standard.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr.so.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libndr.so.1.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libnetapi.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-credentials.so.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-credentials.so.1.0.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-errors.so.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-hostconfig.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-hostconfig.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-passdb.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-passdb.so.0.28.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-util.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamba-util.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamdb.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsamdb.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libsmbconf.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsmbldap.so.2 is empty, not checked.
/sbin/ldconfig: File /lib64/libsmbldap.so.2.1.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libtevent-util.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libtevent-util.so.0.0.1 is empty, not checked.
/sbin/ldconfig: File /lib64/libsmbclient.so.0 is empty, not checked.
/sbin/ldconfig: File /lib64/libsmbclient.so.0.7.0 is empty, not checked.

  Verifying        : epel-release-8-13.el8.noarch                                                                                   1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
```

```
[martint@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:00:23 ago on Tue 14 Dec 2021 03:26:42 PM CET.
remi-release-8.rpm                                                                                       15 kB/s |  26 kB     00:01
Dependencies resolved.
========================================================================================================================================
 Package                          Architecture               Version                             Repository                        Size
========================================================================================================================================
Installing:
 remi-release                     noarch                     8.5-2.el8.remi                      @commandline                      26 k

Transaction Summary
========================================================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                                             1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                                             1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!
```

```
[martint@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                                               3.1 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                                               2.6 MB/s | 947 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                              2.1 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                              3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                              1.5 MB/s | 2.0 MB     00:01
Dependencies resolved.
========================================================================================================================================
 Package                         Architecture                   Version                           Repository                       Size
========================================================================================================================================
Enabling module streams:
 php                                                            remi-7.4

Transaction Summary
========================================================================================================================================

Is this ok [y/N]: y
Complete!
```

[martint@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:38 ago on Tue 14 Dec 2021 03:27:58 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
========================================================================================================================================
 Package                                  Architecture       Version                                        Repository             Size
========================================================================================================================================
Installing:
 php74-php                                x86_64             7.4.26-1.el8.remi                              remi-safe             1.5 M
 php74-php-bcmath                         x86_64             7.4.26-1.el8.remi                              remi-safe              88 k
 php74-php-common                         x86_64             7.4.26-1.el8.remi                              remi-safe             710 k
 php74-php-gd                             x86_64             7.4.26-1.el8.remi                              remi-safe              93 k
 php74-php-gmp                            x86_64             7.4.26-1.el8.remi                              remi-safe              84 k
 php74-php-intl                           x86_64             7.4.26-1.el8.remi                              remi-safe             201 k
 [...]
  php74-php-process-7.4.26-1.el8.remi.x86_64                     php74-php-sodium-7.4.26-1.el8.remi.x86_64
  php74-php-xml-7.4.26-1.el8.remi.x86_64                         php74-runtime-1.0-3.el8.remi.x86_64
  policycoreutils-python-utils-2.9-16.el8.noarch                 python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64                           python3-policycoreutils-2.9-16.el8.noarch
  python3-setools-4.3.0-2.el8.x86_64                             scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```


2. Conf Apache
➜ Le fichier de conf principal utilisé par Apache est /etc/httpd/conf/httpd.conf.
Il y en a plein d'autres : ils sont inclus par le fichier de conf principal.
➜ Dans Apache, il existe la notion de VirtualHost. On définit des VirtualHost dans les fichiers de conf d'Apache.
On crée un VirtualHost pour chaque application web qu'héberge Apache.

"Application Web" c'est le terme de hipster pour désigner un site web. Disons qu'aujourd'hui les sites peuvent faire tellement de trucs qu'on appelle plutôt ça une "application" plutôt qu'un "site". Une application web donc.

➜ Dans le dossier /etc/httpd/ se trouve un dossier conf.d.
Des dossiers qui se terminent par .d, vous en rencontrerez plein (pas que pour Apache) ce sont des dossiers de drop-in.
Plutôt que d'écrire 40000 lignes dans un seul fichier de conf, on éclate la configuration dans plusieurs fichiers.
C'est + lisible et + facilement maintenable.
Les dossiers de drop-in servent à accueillir ces fichiers de conf additionels.
Le fichier de conf principal a une ligne qui inclut tous les autres fichiers de conf contenus dans le dossier de drop-in.

🌞 Analyser la conf Apache

```
IncludeOptional conf.d/*.conf
```

🌞 Créer un VirtualHost qui accueillera NextCloud


```
[martint@web conf.d]$ sudo vi virtualhost.conf
[martint@web conf.d]$ cat virtualhost.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[martint@web conf.d]$ sudo mv virtualhost.conf web.tp2.cesi.conf
[martint@web conf.d]$ ls -al
total 24
drwxr-xr-x. 2 root root  129 Dec 14 16:03 .
drwxr-xr-x. 5 root root  105 Dec 14 14:56 ..
-rw-r--r--. 1 root root 2926 Nov 15 04:13 autoindex.conf
-rw-r--r--. 1 root root 1681 Nov 16 17:40 php74-php.conf
-rw-r--r--. 1 root root  400 Nov 15 04:13 README
-rw-r--r--. 1 root root 1252 Nov 15 04:10 userdir.conf
-rw-r--r--. 1 root root  436 Dec 14 16:01 web.tp2.cesi.conf
-rw-r--r--. 1 root root  574 Nov 15 04:10 welcome.conf
[martint@web conf.d]$
```


🌞 Configurer la racine web


```
[martint@web www]$ ls -al
total 4
drwxr-xr-x.  5 root   root   50 Dec 14 16:07 .
drwxr-xr-x. 22 root   root 4096 Dec 14 14:56 ..
drwxr-xr-x.  2 root   root    6 Nov 15 04:13 cgi-bin
drwxr-xr-x.  2 root   root    6 Nov 15 04:13 html
drwxr-xr-x.  3 apache root   18 Dec 14 16:08 nextcloud

[martint@web www]$ cd nextcloud/
[martint@web nextcloud]$ ls -al
total 0
drwxr-xr-x. 3 apache root 18 Dec 14 16:08 .
drwxr-xr-x. 5 root   root 50 Dec 14 16:07 ..
drwxr-xr-x. 2 apache root  6 Dec 14 16:08 html
```


🌞 Configurer PHP


```
[martint@web nextcloud]$ timedatectl
               Local time: Tue 2021-12-14 16:12:47 CET
           Universal time: Tue 2021-12-14 15:12:47 UTC
                 RTC time: Tue 2021-12-14 15:12:47
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
```


```
[CLI Server]
; Whether the CLI web server uses ANSI color coding in its terminal output.
cli_server.color = On

[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone = "Europe/Paris"

; http://php.net/date.default-latitude
;date.default_latitude = 31.7667

; http://php.net/date.default-longitude
;date.default_longitude = 35.2333

; http://php.net/date.sunrise-zenith
;date.sunrise_zenith = 90.583333

; http://php.net/date.sunset-zenith
;date.sunset_zenith = 90.583333

[filter]
; http://php.net/filter.default
;filter.default = unsafe_raw

; http://php.net/filter.default-flags
;filter.default_flags =
```



III. NextCloud
On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.
🌞 Récupérer Nextcloud

# Petit tip : la commande cd sans argument permet de retourner dans votre homedir
$ cd

# La commande curl -SLO permet de rapidement télécharger un fichier, en HTTP/HTTPS, dans le dossier courant (allez regarder ce que signifient ces options)
# Peut-être connaissiez vous déjà la commande wget. C'est simple : curl peut faire tout ce que fait wget. L'inverse n'est pas vrai.
$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip

$ ls
nextcloud-21.0.1.zip


🌞 Ranger la chambre


```
  249  curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  250  ls -al
  251  sudo dnf install unzip
  252  sudo unzip nextcloud-21.0.1.zip
```

```
[martint@web ~]$ ls -al
total 151628
drwx------.  4 martint martint       162 Dec 14 16:23 .
drwxr-xr-x.  3 root    root           21 Dec 13 10:35 ..
-rw-------.  1 martint martint      1909 Dec 14 13:48 .bash_history
-rw-r--r--.  1 martint martint        18 Jul 27 16:21 .bash_logout
-rw-r--r--.  1 martint martint       141 Jul 27 16:21 .bash_profile
-rw-r--r--.  1 martint martint       376 Jul 27 16:21 .bashrc
-rw-------.  1 martint martint        51 Dec 14 14:53 .mysql_history
drwxr-xr-x. 13 root    root         4096 Apr  8  2021 nextcloud
-rw-rw-r--.  1 martint martint 155240687 Dec 14 16:20 nextcloud-21.0.1.zip
drwx------.  2 martint martint        29 Dec 14 10:28 .ssh
```

```
[martint@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/
[martint@web ~]$ cd /var/www/nextcloud/html/
[martint@web html]$ ls -al
total 4
drwxr-xr-x.  3 apache root   23 Dec 14 16:27 .
drwxr-xr-x.  3 apache root   18 Dec 14 16:08 ..
drwxr-xr-x. 13 root   root 4096 Apr  8  2021 nextcloud
[martint@web html]$ sudo chown apache nextcloud
[martint@web html]$ ls -al
total 4
drwxr-xr-x.  3 apache root   23 Dec 14 16:27 .
drwxr-xr-x.  3 apache root   18 Dec 14 16:08 ..
drwxr-xr-x. 13 apache root 4096 Apr  8  2021 nextcloud
```



4. Test
Bah on arrive sur la fin du setup !
Si on résume :


un serveur de base de données : db.tp2.cesi

MariaDB installé et fonctionnel
firewall configuré
une base de données et un user pour NextCloud ont été créés dans MariaDB



un serveur Web : web.tp2.cesi

Apache installé et fonctionnel
firewall configuré
un VirtualHost qui pointe vers la racine /var/www/nextcloud/html/

NextCloud installé dans le dossier /var/www/nextcloud/html/




Looks like we're ready.

Ouuu presque. Pour que NextCloud fonctionne correctement, il est préférable d'y accéder en utilisant un nom, et pas une IP.
On va donc devoir faire en sorte que, depuis votre PC, vous puissiez écrire http://web.tp2.cesi plutôt que http://10.2.1.11.
➜ Pour faire ça, on va utiliser le fichier hosts. C'est un fichier présents sur toutes les machines, sur tous les OS.
Il sert à définir, localement, une correspondance entre une IP et un ou plusieurs noms.
Emplacement du fichier hosts :

MacOS/Linux : /etc/hosts

Windows : c:\windows\system32\drivers\etc\hosts



🌞 Modifiez le fichier hosts de votre PC


```
192.168.171.2 web.tp2.cesi
```


🌞 Tester l'accès à NextCloud et finaliser son install'

ouvrez votre navigateur Web sur votre PC
rdv à l'URL http://web.tp2.cesi

vous devriez avoir la page d'accueil de NextCloud
ici deux choses :

les deux champs en haut pour créer un user admin au sein de NextCloud
le bouton "Configure the database" en bas

sélectionnez "MySQL/MariaDB"
entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
c'est les infos avec lesquelles vous avez validé à la main le bon fonctionnement de MariaDB (c'était avec la commande mysql)






🔥🔥🔥 Baboom ! Un beau NextCloud.
Naviguez un peu, faites vous plais', vous avez votre propre DropBox.

Ptet le moment de prendre une pause pour laisser votre cerveau respirer ? :)

Et après, go next : Partie II : Sécurisation











1. Conf SSH
Si c'est pas fait vous devez maintenant configurer un accès par clés pour SSH.
🌞 Modifier la conf du serveur SSH

```
[martint@web ssh]$ sudo cat sshd_config
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

[...]

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes
```


```
65 #IgnoreRhosts yes
     66
     67 # To disable tunneled clear text passwords, change to no here!
     68 #PasswordAuthentication yes
     69 #PermitEmptyPasswords no
     70 PasswordAuthentication no
     71
     72 # Change to no to disable s/key passwords
     73 #ChallengeResponseAuthentication yes
     74 ChallengeResponseAuthentication no
     75
     76 # Kerberos options
     77 #KerberosAuthentication no
     78 #KerberosOrLocalPasswd yes
     79 #KerberosTicketCleanup yes

```

```
# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctrhostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256
```







2. Bonus : Fail2Ban
Fail2Ban est un outil très simple :

il lit les fichiers de logs en temps réel
il repère des lignes en particulier, en fonction de patterns qu'on a défini
si des lignes correspondant au même pattern sont répétées trop de fois, il effectue une action

Le cas typique :

serveur SSH
get attacked :(
fail2ban détecte l'attaque rapidement, et ban l'IP source

Ca permet de se prémunir là encore d'attaques de masse : il faut que ça spam pour que fail2ban agisse. Des attaques qui essaient de bruteforce l'accès typiquement.

C'est pas du tout que dans les séries Netflix hein. Vous mettez une machine avec une IP publique sur internet et c'est très rapide, vous vous faites attaquer toute la journée, tout le temps, plusieurs fois par minutes c'est pas choquant. Plus votre IP reste longtemps avec tel ou tel port ouvert, plus elle est "connue", et les attaques n'iront pas en diminuant.

🌞 Installez et configurez fail2ban

```
[martint@web jail.d]$ sudo dnf install fail2ban fail2ban-firewalld -y
```
```
# "bantime" is the number of seconds that a host is banned.
bantime  = 1h

# A host is banned if it has generated "maxretry" during the last "findtime"
# seconds.
findtime  = 1h

# "maxretry" is the number of failures before a host get banned.
maxretry = 5
```


```
[martint@web fail2ban]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
```

```
[martint@web jail.d]$ cat sshd.local
[sshd]
enabled = true
bantime = 1d
maxretry = 3
```

II. Serveur Web
Bah ui certains l'ont fait en bonus hier, mais comme ça, on met tout le monde à niveau. Libre à vous de sauter ça ou de le refaire (un peu plus en conscience avec le cours sur TLS ?) ou simplement de le faire si c'était pas fait avant !

1. Reverse Proxy
Un reverse proxy c'est un serveur qui sert d'intermédiaire entre un client et un serveur, il est mis en place par l'administrateur du serveur.
Il a pour simple rôle de réceptionner les requêtes du client, et de les faire passer au serveur. Nice, easy, and simple.
Maiiiis il peut évidemment permettre + de choses que ça :

chiffrement (sécu, perfs)
caching (perfs)
répartition de charge (sécu)

De façon générale, c'est lui qui est le "front" des applications Web, faut que ce soit une machine robuste et secure. Il permet de centraliser les accès des clients aux applications Web.
En effet, il n'est pas rare dans l'industrie de voir un nombre restreint de reverse proxies (mais bien bien solides) qui permettent d'accéder à une multitudes d'applications Web.
N'hésitez pas, comme d'hab, à m'appeler si vous voulez une explication claire à l'oral de tout ça.

🖥️ Créez une nouvelle machine : proxy.tp2.cesi. 🖥️

vous déroulerez la checklist de la section prérequis du TP sur cette nouvelle machine

🌞 Installer NGINX


```
[martint@proxy ~]$ sudo dnf install nginx
[sudo] password for martint:
Last metadata expiration check: 1 day, 1:30:20 ago on Tue 14 Dec 2021 10:34:36 AM CET.
Dependencies resolved.
====================================================================================================================
 Package                           Architecture Version                                       Repository       Size
====================================================================================================================
Installing:
 nginx                             x86_64       1:1.14.1-9.module+el8.4.0+542+81547229        appstream       566 k
Installing dependencies:
 fontconfig                        x86_64       2.13.1-4.el8                                  baseos          273 k
 gd                                x86_64       2.2.5-7.el8                                   appstream       143 k
 jbigkit-libs                      x86_64       2.1-14.el8                                    appstream        54 k
[...]
  perl-libs-4:5.26.3-420.el8.x86_64
  perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!
```

Quelques infos :

les fichiers de log sont dans le dossier /var/log/nginx/

la conf principale est dans /etc/nginx/nginx.conf

une des dernières lignes inclut des fichiers contenus dans /etc/nginx/conf.d/

n'hésitez pas à le lire le fichier, et à l'épurer des commentaires inutiles pour y voir plus clair


n'oubliez pas la commande sudo journalctl -xe -u <SERVICE> pour obtenir les logs d'un service donné

🌞 Configurer NGINX comme reverse proxy


```
[martint@proxy conf.d]$ pwd
/etc/nginx/conf.d
[martint@proxy conf.d]$ cat cesi.conf
upstream web.tp2.cesi {
    server 192.168.171.2;
}

server {
    server_name web.tp2.cesi;

    location / {
        proxy_pass http://web.tp2.cesi;
        proxy_set_header    Host $host;

        proxy_connect_timeout 30;
        proxy_send_timeout 30;
    }
}
[martint@proxy conf.d]$
```





🌞 Une fois en place, text !


```
192.168.171.5 web.tp2.cesi
```

(192.168.171.5 étant l'adresse ip de mon proxy.tp2.cesi)


```
C:\Users\Thomas>ping web.tp2.cesi

Envoi d’une requête 'ping' sur web.tp2.cesi [192.168.171.5] avec 32 octets de données :
Réponse de 192.168.171.5 : octets=32 temps<1ms TTL=64
Réponse de 192.168.171.5 : octets=32 temps<1ms TTL=64
Réponse de 192.168.171.5 : octets=32 temps<1ms TTL=64
Réponse de 192.168.171.5 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.171.5:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```



2. HTTPS
🌞 Générer une clé et un certificat avec la commande suivante :


```
[martint@web fail2ban]$ sudo openssl req -new -newkey  rsa:4096 -days 365 -nodes -x509 -keyout web.tp2.cesi.key -out
 web.tp2.cesi.crt
[sudo] password for martint:
Generating a RSA private key
......................................................................................................................................................................................................................................++++
.....++++
writing new private key to 'web.tp2.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Gironde
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:IT
Common Name (eg, your name or your server's hostname) []:web.tp1.cesi
Email Address []:thomas.martinez@cesi.fr
```


🌞 Allez, faut ranger la chambre

```
[martint@proxy ~]$ sudo mv web.tp2.cesi.key /etc/pki/tls/private/
[martint@proxy ~]$ sudo mv web.tp2.cesi.crt /etc/pki/tls/certs/
[martint@proxy ~]$ ls /etc/pki/tls/certs/
ca-bundle.crt  ca-bundle.trust.crt  web.tp2.cesi.crt
[martint@proxy ~]$ ls /etc/pki/tls/private/
web.tp2.cesi.key
```



🌞 Affiner la conf de NGINX


```
[martint@proxy certs]$ cat /etc/nginx/conf.d/cesi.conf
upstream web.tp2.cesi {
    server 192.168.171.2;
}

server {
        listen  443 ssl http2;
        server_name web.tp2.cesi;


        ssl_certificate /etc/pki/tls/certs/web.tp2.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/web.tp2.cesi.key;



    location / {
        proxy_pass http://web.tp2.cesi;
        proxy_set_header    Host $host;

        proxy_connect_timeout 30;
        proxy_send_timeout 30;



    }
}

[martint@proxy certs]$ sudo systemctl restart nginx.service
[martint@proxy certs]$ sudo systemctl status nginx.service
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-15 14:53:46 CET; 6s ago
  Process: 5274 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 5273 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 5271 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 5276 (nginx)
    Tasks: 3 (limit: 17672)
   Memory: 5.1M
   CGroup: /system.slice/nginx.service
           ├─5276 nginx: master process /usr/sbin/nginx
           ├─5277 nginx: worker process
           └─5278 nginx: worker process
```



```
[martint@proxy certs]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[martint@proxy certs]$ sudo firewall-cmd --reload
success
```


```
[martint@proxy certs]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp 443/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```



Eeet bah c'est tout.
🌞 Test !









🌞 Bonus


ajouter le certificat au magasin de certificat de votre navigateur pour avoir un pitit cadenas vert

très utilisé en entreprise, avec des méthodes automatisées bien sûr : déployer un cert interne sur les postes client pour trust les applications internes



ajouter une redirection HTTP -> HTTPS

c'est très courant aussi : on fait en sorte que si un client arrive en HTTP, il soit redirigé vers le site en HTTPS
ça permet aux clients qui arrivent en HTTP de pas manger un gros "CONNECTION REFUSED" dans leur navigateur :)


hop un artiiiicle qui parle de ça (n'hésitez pas, comme toujours, à chercher par vous-mêmes)





renforcer l'échange TLS en sélectionnant les algos de chiffrement à utiliser

cherchez "nginx strong ciphers" :)



répartition de charge

clonez la VM Apache + NextCloud
mettez en place de la répartition de charge entre les deux machines renommées web1.tp2.cesi et web2 .tp2.cesi







Setup Netdata

Sur quelle machine ? A terme, toutes dans l'idéale, je ne vous l'imposerai pas ici, libre à vous :) Commencez au moins par une, pour prendre en main.

🌞 Installez Netdata en exécutant la commande suivante :


```
[martint@web ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-GeNFosRCFv]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-GeNFosRCFv/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK

[...]

[/opt/netdata]# chmod 0644 /opt/netdata/etc/netdata/netdata.conf
 OK

 OK

[/tmp/netdata-kickstart-GeNFosRCFv]$ sudo rm /tmp/netdata-kickstart-GeNFosRCFv/netdata-latest.gz.run
 OK

[/tmp/netdata-kickstart-GeNFosRCFv]$ sudo rm -rf /tmp/netdata-kickstart-GeNFosRCFv
 OK
```



🌞 Démarrez Netdata


```
[martint@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
[martint@web ~]$ sudo firewall-cmd --reload
[martint@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp 19999/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

```



3. Bonus : alerting
🌞 Mettez en place un alerting Discord



```
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/920686005533675530/0GArPGa_ZswKiL-_OBmJxD4LQujvGsargr3srI1QegEuXNvi7c38a4rqCYEi1Kzlyu--"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD=

```



4. Bonus : proxying
Euh bah là on a Apache protégé par NGINX, mais le serveur Web à moitié naze que Netdata utilise, pas de soucis ? BAH SI c'est un soucis :'(
🌞 Ajustez la conf

l'interface web de Netdata doit être joignable par le reverse proxy


II. Backup
Borg est un outil qui permet de réaliser des sauvegardes.
Il est performant et sécurisé :

performant à l'utilisation

création de backup rapide, idem pour restauration


les données sauvegardées sont chiffrées
les données sauvegardées sont dédupliquées

Il repose sur un concept simple :

on créer un "dépôt Borg" sur la machine avec une commande borg init

c'est dans ce dépôt que pourront être stockées des sauvegardes
on peut ensuite déclencher une sauvegarde d'un dossier donné vers le dépôt avec une commande borg create


Nous, on va écrire un script :

il créer une nouvelle sauvegarde borg (avec borg create donc) d'un dossier précis, avec un nommage précis, dans un dépôt précis
on en fera ensuite un service : un p'tit backup.service

puis on fera en sorte que ce service se lance à intervalles réguliers

🌞 Téléchargez Borg sur la machine web.tp2.cesi

téléchargez avec les commandes suivantes :



```
[martint@web ~]$ curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   649  100   649    0     0   1478      0 --:--:-- --:--:-- --:--:--  1478
100 18.0M  100 18.0M    0     0  1932k      0  0:00:09  0:00:09 --:--:-- 3105k
[martint@web ~]$ sudo cp borg-linux64 /usr/l
lib/     lib64/   libexec/ local/
[martint@web ~]$ sudo cp borg-linux64 /usr/local/bin/borg
[sudo] password for martint:
[martint@web ~]$ sudo chown root:root /usr/l
lib/     lib64/   libexec/ local/
[martint@web ~]$ sudo chown root:root /usr/local/bin/borg
[martint@web ~]$ sudo chmod 755 /usr/local/bin/borg
[martint@web ~]$ cd /usr/local/bin/
[martint@web bin]$ ls -al
total 18500
drwxr-xr-x.  2 root root       18 Dec 16 09:12 .
drwxr-xr-x. 12 root root      131 Dec 13 10:32 ..
-rwxr-xr-x.  1 root root 18943688 Dec 16 09:12 borg

```

🌞 Jouer avec Borg

```
[martint@web bin]$ sudo chown martint backup1/
[martint@web bin]$ ls -al
total 18500
drwxr-xr-x.  3 root    root       33 Dec 16 09:26 .
drwxr-xr-x. 12 root    root      131 Dec 13 10:32 ..
drwxr-xr-x.  2 martint root        6 Dec 16 09:26 backup1
-rwxr-xr-x.  1 root    root 18943688 Dec 16 09:12 borg
[martint@web bin]$ borg init --encryption=repokey /usr/local/bin/backup1/
Enter new passphrase:
Enter same passphrase again:
Do you want your passphrase to be displayed for verification? [yN]: y
Your passphrase (between double-quotes): "dbc"
Make sure the passphrase displayed above is exactly what you wanted.

By default repositories initialized with this version will produce security
errors if written to with an older version (up to and including Borg 1.0.8).

If you want to use these older versions, you can disable the check by running:
borg upgrade --disable-tam /usr/local/bin/backup1

See https://borgbackup.readthedocs.io/en/stable/changes.html#pre-1-0-9-manifest-spoofing-vulnerability
for details about the security implications.

IMPORTANT: you will need both KEY AND PASSPHRASE to access this repo!
If you used a repokey mode, the key is stored in the repo, but you should back it up separately.
Use "borg key export" to export the key, optionally in printable format.
Write down the passphrase. Store both at safe place(s).
```



🌞 Ecrire un script

il doit sauvegarder le dossier où se trouve NextCloud
le dépôt borg doit être dans le dossier /srv/backup/

le nom de la sauvegarde doit être nextcloud_YYMMDD_HHMMSS

testez le à la main, avant de continuer

🌞 Créer un service

créer un service backup_db.service qui exécute votre script
ainsi, quand on lance le service, une backup de la base de données est déclenchée

La syntaxe, toujours la même :

[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>
Type=oneshot

[Install]
WantedBy=multi-user.target


NB : vous DEVEZ ajoutez la ligne Type=oneshot en dessous de la ligne ExecStart= dans votre service. Cela indique au système que ce service ne sera pas un démon qui s'exécute en permanence, mais un script s'eécutera, puis qui aura une fin d'exécution (sinon le système peut par exemple essayer de relancer automatiquement un service qui s'arrête).
🌞 Créer un timer

un timer c'est un fichier qui permet d'exécuter un service à intervalles réguliers
créez un timer qui exécute le service backup toutes les heures

Pour cela, créer le fichier /etc/systemd/system/backup.timer.

Notez qu'il est dans le même dossier que le service, et qu'il porte le même nom, mais pas la même extension.

Contenu du fichier /etc/systemd/system/backup.timer :

[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target



Au niveau du OnCalendar, on précise tout un tas de trucs : une heure précise une fois par semaine, toutes les trois heures les jours pairs (ui c'est improbable, mais on peut !), etc.

Activez maintenant le timer avec :

# on indique qu'on a modifié la conf du système
$ sudo systemctl daemon-reload

# démarrage immédiat du timer
$ sudo systemctl start backup.timer

# activation automatique du timer au boot de la machine
$ sudo systemctl enable backup.timer


🌞 Vérifier que le timer a été pris en compte, en affichant l'heure de sa prochaine exécution :

$ sudo systemctl list-timers