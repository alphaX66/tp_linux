# Création d'un utilisateur

```
[martint@template ~]$ sudo useradd thomas -G wheel
[martint@template ~]$ passwd thomas
passwd: Only root can specify a user name.
[martint@template ~]$ sudo passwd thomas
Changing password for user thomas.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
```


```
[martint@template ~]$ ll /home/
total 0
drwx------. 2 martint martint 83 Dec 13 11:24 martint
drwx------. 2 thomas  thomas  62 Dec 13 12:09 thomas
```

```
[martint@tp1 ~]$ su thomas
Password:
[thomas@tp1 martint]$ groups
thomas wheel
```


# Analyse du service SSH

```
[thomas@tp1 martint]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 11:58:45 CET; 42min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 925 (sshd)
    Tasks: 1 (limit: 17672)
   Memory: 4.7M
   CGroup: /system.slice/sshd.service
           └─925 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256->

Dec 13 11:58:45 template.cesi systemd[1]: Starting OpenSSH server daemon...
Dec 13 11:58:45 template.cesi sshd[925]: Server listening on 0.0.0.0 port 22.
Dec 13 11:58:45 template.cesi sshd[925]: Server listening on :: port 22.
Dec 13 11:58:45 template.cesi systemd[1]: Started OpenSSH server daemon.
Dec 13 11:59:54 template.cesi sshd[1559]: Accepted password for martint from 192.168.171.1 port 58485 ssh2
Dec 13 11:59:54 template.cesi sshd[1559]: pam_unix(sshd:session): session opened for user martint by (uid=0)
Dec 13 12:12:07 tp1.cesi sshd[1682]: Accepted password for martint from 192.168.171.1 port 58675 ssh2
Dec 13 12:12:07 tp1.cesi sshd[1682]: pam_unix(sshd:session): session opened for user martint by (uid=0)
```




```
[martint@tp1 ~]$ sudo ss -luptn
[sudo] password for martint:
Netid   State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
tcp     LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=925,fd=5))
tcp     LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=925,fd=7))
```



```
[martint@tp1 ~]$ ps -aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.4 174604 13112 ?        Ss   11:58   0:01 /usr/lib/systemd/systemd --switched-root --system
root           2  0.0  0.0      0     0 ?        S    11:58   0:00 [kthreadd]
[...]
root         937  0.0  0.2 137300  6356 ?        Ss   11:58   0:00 login -- martint
root         939  0.0  0.0  44004  2496 ?        Ss   11:58   0:00 /usr/sbin/atd -f
martint     1514  0.0  0.3  89448  9228 ?        Ss   11:58   0:00 /usr/lib/systemd/systemd --user
martint     1518  0.0  0.1 162192  5016 ?        S    11:58   0:00 (sd-pam)
martint     1525  0.0  0.1 235076  5124 tty1     Ss+  11:58   0:00 -bash
root        1637  0.0  0.0      0     0 ?        I    12:04   0:00 [kworker/1:0-mm_percpu_wq]
root        1654  0.0  0.0      0     0 ?        R    12:09   0:00 [kworker/u256:0-events_unbound]
root        1682  0.0  0.3 153448 10248 ?        Ss   12:12   0:00 sshd: martint [priv]
martint     1686  0.0  0.1 153448  5576 ?        S    12:12   0:00 sshd: martint@pts/0
```


# Modification du service SSH


```
[martint@tp1 ~]$ sudo vi /etc/ssh/sshd_config

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
#Port 22
Port 6666
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

```


```
[martint@tp1 ~]$ sudo firewall-cmd --permanent --add-port=6666/tcp
```

```
[martint@tp1 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 13:05:18 CET; 7s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 2130 (sshd)
    Tasks: 1 (limit: 17672)
   Memory: 1.1M
   CGroup: /system.slice/sshd.service
           └─2130 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256>

Dec 13 13:05:18 tp1.cesi systemd[1]: Starting OpenSSH server daemon...
Dec 13 13:05:18 tp1.cesi sshd[2130]: Server listening on 0.0.0.0 port 6666.
Dec 13 13:05:18 tp1.cesi sshd[2130]: Server listening on :: port 6666.
Dec 13 13:05:18 tp1.cesi systemd[1]: Started OpenSSH server daemon.

```

```

PS C:\Users\Thomas> ssh martint@192.168.171.2 -p 6666
martint@192.168.171.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec 13 12:42:14 2021
[martint@tp1 ~]$

```

# Service Web

```
[martint@tp1 ~]$ sudo dnf install nginx
[sudo] password for martint:
Last metadata expiration check: 0:13:53 ago on Mon 13 Dec 2021 12:58:55 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                           Architecture Version                                        Repository       Size
=====================================================================================================================
                               927 kB/s |  36 kB     00:00
[...]
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!

```



```

[martint@tp1 ~]$ sudo systemctl start nginx
[martint@tp1 ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 13:14:23 CET; 12s ago
  Process: 26465 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 26463 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 26461 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 26466 (nginx)
    Tasks: 3 (limit: 17672)
   Memory: 5.1M
   CGroup: /system.slice/nginx.service
           ├─26466 nginx: master process /usr/sbin/nginx
           ├─26467 nginx: worker process
           └─26468 nginx: worker process

Dec 13 13:14:23 tp1.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 13 13:14:23 tp1.cesi nginx[26463]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 13 13:14:23 tp1.cesi nginx[26463]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 13 13:14:23 tp1.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.

```


```

[martint@tp1 home]$ sudo ss -lutpn
[sudo] password for martint:
Netid        State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port        Process
tcp          LISTEN        0             128                        0.0.0.0:6666                    0.0.0.0:*            users:(("sshd",pid=2130,fd=4))
tcp          LISTEN        0             128                        0.0.0.0:80                      0.0.0.0:*            users:(("nginx",pid=26468,fd=8),("nginx",pid=26467,fd=8),("nginx",pid=26466,fd=8))
tcp          LISTEN        0             128                           [::]:6666                       [::]:*            users:(("sshd",pid=2130,fd=6))
tcp          LISTEN        0             128                           [::]:80                         [::]:*            users:(("nginx",pid=26468,fd=9),("nginx",pid=26467,fd=9),("nginx",pid=26466,fd=9))


[martint@tp1 home]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[martint@tp1 home]$ sudo firewall-cmd --reload
success

```

```

[martint@tp1 home]$ curl 192.168.171.2:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
[...}
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>

      </div>
    </div>
  </body>
</html>

```


# Votre propre service

```

[martint@tp1 home]$ sudo firewall-cmd --permanent --add-port=8888/tcp
[sudo] password for martint:
success
[martint@tp1 home]$ sudo firewall-cmd --reload
success
[martint@tp1 home]$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.171.1 - - [13/Dec/2021 13:48:41] "GET / HTTP/1.1" 200 -
192.168.171.1 - - [13/Dec/2021 13:48:41] code 404, message File not found
192.168.171.1 - - [13/Dec/2021 13:48:41] "GET /favicon.ico HTTP/1.1" 404 -
192.168.171.1 - - [13/Dec/2021 13:48:41] "GET / HTTP/1.1" 200 -

```

```

[martint@tp1 home]$ sudo dnf install python39
[sudo] password for martint:
Last metadata expiration check: 0:37:00 ago on Mon 13 Dec 2021 12:58:55 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                           Architecture   Version                                       Repository         Size
========================================================================================================================
Installing:
 python39                          x86_64         3.9.6-2.module+el8.5.0+673+10283621           appstream          31 k
Installing dependencies:
 python39-libs                     x86_64         3.9.6-2.module+el8.5.0+673+10283621           appstream         8.2 M
 python39-pip-wheel                noarch         20.2.4-6.module+el8.5.0+673+10283621          appstream         1.3 M
 python39-setuptools-wheel         noarch         50.3.2-4.module+el8.5.0+673+10283621          appstream         496 k
Installing weak dependencies:
 python39-pip                      noarch         20.2.4-6.module+el8.5.0+673+10283621          appstream         2.0 M
 python39-setuptools               noarch         50.3.2-4.module+el8.5.0+673+10283621          appstream         870 k
Enabling module streams:
 python39                                         3.9

[...]

Installed:
  python39-3.9.6-2.module+el8.5.0+673+10283621.x86_64
  python39-libs-3.9.6-2.module+el8.5.0+673+10283621.x86_64
  python39-pip-20.2.4-6.module+el8.5.0+673+10283621.noarch
  python39-pip-wheel-20.2.4-6.module+el8.5.0+673+10283621.noarch
  python39-setuptools-50.3.2-4.module+el8.5.0+673+10283621.noarch
  python39-setuptools-wheel-50.3.2-4.module+el8.5.0+673+10283621.noarch

Complete!

```


```
[martint@tp1 system]$ [martint@tp1 system]$ sudo vi web.service
[Unit]
Description="Lancer le serveur Python"

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

```

```
[martint@tp1 system]$ which python3
/usr/bin/python3
[martint@tp1 system]$ sudo systemctl daemon-reload
[martint@tp1 system]$ sudo systemctl status web.service
● web.service - "Lancer le serveur Python"
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[martint@tp1 system]$ sudo systemctl start web.service
[martint@tp1 system]$ sudo systemctl status web.service
● web.service - "Lancer le serveur Python"
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 14:00:02 CET; 2s ago
 Main PID: 27472 (python3)
    Tasks: 1 (limit: 17672)
   Memory: 9.3M
   CGroup: /system.slice/web.service
           └─27472 /usr/bin/python3 -m http.server 8888

Dec 13 14:00:02 tp1.cesi systemd[1]: Started "Lancer le serveur Python".
[martint@tp1 system]$ sudo systemctl enable web.service
Password:

```




```

[martint@tp1 system]$ sudo useradd web
[martint@tp1 system]$ passwd web
passwd: Only root can specify a user name.
[martint@tp1 system]$ sudo passwd web
Changing password for user web.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.

```


```

[martint@tp1 system]$ sudo mkdir /srv/web/
[martint@tp1 system]$ sudo vi /srv/web/text
[martint@tp1 system]$ sudo chown web /srv/web/text
[martint@tp1 web]$ ls -al
total 4
drwxr-xr-x. 2 root root 18 Dec 13 14:06 .
drwxr-xr-x. 3 root root 17 Dec 13 14:06 ..
-rw-r--r--. 1 web  root 37 Dec 13 14:06 text
```

```

[martint@tp1 web]$ sudo vi /etc/systemd/system/web.service
[Unit]
Description="Lancer le serveur Python"

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkindDirectory=/srv/web/

[Install]
WantedBy=multi-user.target

[martint@tp1 web]$ sudo systemctl daemon-reload

```



# BONUS

```

PS C:\Users\Thomas> ssh-keygen -b 4096
[martint@tp1 ~]$sudo vi /home/martint/.ssh/authorized_keys
[martint@tp1 ~]$ chmod 600 authorized_keys
PS C:\Users\Thomas> ssh martint@192.168.171.2 -p 6666
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec 13 14:52:21 2021 from 192.168.171.1
[martint@tp1 ~]$

```




```

[martint@tp1 html]$ sudo mv index.html index.html.old
[sudo] password for martint:
[martint@tp1 html]$ ls
404.html  50x.html  index.html.old  nginx-logo.png  poweredby.png

[martint@tp1 html]$ curl 192.168.171.2:80
<html>
<head><title>403 Forbidden</title></head>
<body bgcolor="white">
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.14.1</center>
</body>
</html>

```